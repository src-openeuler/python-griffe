%global pypi_name griffe

Name:           python-%{pypi_name}
Version:        1.5.1
Release:        1
Summary:        Signatures for entire Python programs. Extract the structure, the frame, the skeleton of your project, to generate API documentation or find breaking changes in your API.
License:        ISC
URL:            https://mkdocstrings.github.io/griffe/
Source0:        https://files.pythonhosted.org/packages/source/g/griffe/griffe-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:	python3-hatchling
BuildRequires:	python3-pdm-pep517
BuildRequires:	python3-pdm-backend

%description
Signatures for entire Python programs. Extract the structure, the frame, the skeleton of your project, to generate API documentation or find breaking changes in your API.

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}
%description -n python3-%{pypi_name}
Signatures for entire Python programs. Extract the structure, the frame, the skeleton of your project, to generate API documentation or find breaking changes in your API.

%package help
Summary:	%{summary}
Provides:	python3-%{pypi_name}-doc
%description help
Signatures for entire Python programs. Extract the structure, the frame, the skeleton of your project, to generate API documentation or find breaking changes in your API.

%prep
%autosetup -n %{pypi_name}-%{version} 

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.md
%{python3_sitelib}/*
%{_bindir}/griffe

%files help
%{_docdir}/* 

%changelog
* Mon Nov 04 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 1.5.1-1
- Update package to version 1.5.1
  * Sort Git tags using creatordate field, which works with both lightweight and annotated tags
  * Allow setting and deleting parameters within container

* Sun Feb 25 2024 wangjunqi <wangjunqi@kylinos.cn> - 0.40.1-1
- Update package to version 0.40.1
  Bug Fixes: Don't return properties as parameters of dataclasses (5a5c03b by Timothée Mazzucotelli). Issue #232

* Thu Dec 07 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 0.38.1-1
- Update package to version 0.38.1

* Tue Aug 29 2023 luolu12 <luluoc@isoftstone.com> - 0.35.0-1
- Initial package.
